# -*- coding: utf-8 -*-
import logging
import pymssql
import calendar
from datetime import datetime

from pyafipws.wsaa import WSAA
from pyafipws.ws_sr_padron import WSSrPadronA4, WSSrPadronA5

from odoo import models, fields, api
from odoo.addons.queue_job.job import job
from odoo.exceptions import ValidationError, UserError, Warning, currentframe

from odoo.addons.afip.lib.task import actualizar_registros

_logger = logging.getLogger(__name__)


class Servicio(models.Model):
    _name = 'afip.service'
    _description = "Service Name"
    _rec_name = 'alias_id'


    servicio = fields.Char(string="Servicio Afip", required=True)
    alias_id = fields.Many2one('afip.alias', string="Alias", required=True)
    url_login = fields.Char(string="Url Login")
    padron_cuit = fields.Char(string="Padron Cuit")
    padron_url = fields.Char(string="Padron Url")
    active = fields.Boolean(string="Active",
            help="Activate or deactivate record", default=False)

    _sql_constraints = [
            ('alias_id_uniq', 'unique(alias_id)', 'Alias already exists'),
            ]

    def _validate_webservice(self):
        self.URL = self.url_login
        self.CERTIFICADO = self.alias_id.path_crt
        self.CLAVEKEY = self.alias_id.path_key
        self.SERVICIO = self.alias_id.alias
        self.PADRON_CUIT = self.padron_cuit
        self.PADRON_URL = self.padron_url
        # raise ValidationError("The service does not exist or the module is not configured")

        self.wsaa = WSAA()
        ta = self.wsaa.Autenticar(self.SERVICIO, self.CERTIFICADO, self.CLAVEKEY, self.URL)

        ticket_creado = False
        self.padron = WSSrPadronA5()  # by default

        # Debido al fallo de padronA4 se harcodea a padronA5
        if self.SERVICIO == 'ws_sr_padron_a5':
            self.padron = WSSrPadronA5()
        elif self.SERVICIO == 'ws_sr_padron_a5':
            self.padron = WSSrPadronA5()

        self.padron.Token = self.wsaa.Token
        self.padron.Sign = self.wsaa.Sign
        self.padron.HOMO = False

        ticket_creado = self.padron.SetTicketAcceso(ta)
        if ticket_creado:
            self.padron.Cuit = self.PADRON_CUIT
            self.padron.Conectar("", self.PADRON_URL)
            ticket_creado = True
        return ticket_creado

    def parse_padron_datos_afip(self):
        lista_valores = []
        lista_cabecera = ["denominacion",
        "cuit",
        "tipo_persona",
        "tipo_doc",
        "dni",
        "estado",
        "direccion",
        "localidad",
        "provincia",
        "cod_postal",
        "impuestos",
        "actividades",
        "imp_iva",
        "cat_iva",
        "monotributo",
        "actividad_monotributo",
        "empleador"]

        lista_valores = [
                self.padron.denominacion.encode('utf8'),
                self.padron.cuit,
                self.padron.tipo_persona,
                self.padron.tipo_doc,
                self.padron.dni,
                self.padron.estado,
                self.padron.direccion,
                self.padron.localidad,
                self.padron.provincia,
                self.padron.cod_postal,
                self.padron.impuestos,
                self.padron.actividades,
                self.padron.imp_iva,
                self.padron.cat_iva,
                self.padron.monotributo,
                self.padron.actividad_monotributo,
                self.padron.empleador]
        return dict(zip(lista_cabecera, lista_valores))

    def consulta_afip_id(self, id_persona):
        response = ''
        ok = False

        if self._validate_webservice():
            try:
                ok = self.padron.Consultar(id_persona)
            except:
                pass

            if ok:
                response = self.parse_padron_datos_afip()
        return response

    @api.one
    def instancia_padrona4(self):
        response = ''
        ok = False

        if self._validate_webservice():
            try:
                ok = self.padron
            except:
                pass

            if ok:
                response = ok
        return response


class Alias(models.Model):
    _name = 'afip.alias'
    _description = 'Alias in AFIP'
    _rec_name = 'alias'

    alias = fields.Char(string="Alias in Afip",
            help="Enter the associated alias in the Afip", required=True)
    description = fields.Text(string="Description", size=150,
            help="Description of service")
    path_crt = fields.Char(string="Path File Crt", help="Enter path file")
    path_key = fields.Char(string="Path File Key", help="Enter path file")
    homo = fields.Boolean(string="Alias is Homo?", default=True)
    active = fields.Boolean(string="Active",
            help="Activate or deactivate record", default=False)


class DbConexiones(models.Model):
    _name = 'afip.dbconection'
    _description = 'Databases Conections'
    _rec_name = 'database'

    host = fields.Char(string="Host", required=True)
    user = fields.Char(string="User", required=True)
    password = fields.Char(string="Password", required=True)
    database = fields.Char(string="Name Database", required=True)
    description = fields.Text(string="Description", required=True)
    active = fields.Boolean(string="Active",
            help="Activate or deactivate record", default=True)

    _sql_constraints = [
            ('database_uniq', 'UNIQUE(host, database)',
                'Database already exist'), ]

    @api.multi
    def name_get(self):
        result = []
        for dbconection in self:
            name = u"{0}-{1}".format(dbconection.host, dbconection.database)
            result.append((dbconection.id, name))
        return result

    @api.one
    def test_conection(self):
        response = []
        try:
            conn = pymssql.connect(self.host, self.user, self.password,
                    self.database)
            response = True, 'Conexión exitosa'
        except pymssql.Error as e:
            response = False, e
        return response

class AuxControlAfip(models.Model):
    _name = 'afip.aux_controlafip'
    _description = 'Copia de la tabla en SQL Server CTSMA.dbo.aux_controlAFIP'
    _rec_name = 'idpersona_jsat'

    idpersona_jsat = fields.Integer()
    numerocliente_jsat = fields.Integer()
    tipopersona_jsat = fields.Char()
    nombrecliente_jsat = fields.Char()
    numerocuit_jsat = fields.Char()
    idcondicioniva_jsat = fields.Integer()
    codigocondicioniva_jsat = fields.Char()
    condicioniva_jsat = fields.Char()
    estadoclave_afip = fields.Char()
    nombrecliente_afip = fields.Char()
    codigocondicioniva_afip = fields.Integer()
    condicioniva_afip = fields.Char()
    tipopersona_afip = fields.Char()
    monotributo_afip = fields.Char()
    actividadmonotributo_afip = fields.Char()
    success = fields.Char()
    fechaproceso = fields.Datetime()

    @job
    def cotesmanet_afip_update(self):
        env_servicio = self.env['afip.service'].search([
            ('alias_id', '=', 'ws_sr_padron_a5'),
            ('alias_id.homo', '=', False),
            ('alias_id.active', '=', 'True')
            ])

        actualizar_registros(env_servicio)

    def ejecutar_store_proc(self):
        db = self.env['afip.dbconection'].search([
            ('active', '=', 'True'),
            ('host', 'ilike', 'pandora'),
            ('database', 'ilike', 'ctsma')], limit=1)

        conn = pymssql.connect(db.host, db.user, db.password, db.database)
        cursor = conn.cursor()
        cursor.callproc('ADM_insert_aux_ControlAfip')
        conn.commit()
        conn.close()

    def _categoria_iva(self, cat_iva):
        if cat_iva == 1:
            response = "IVA Responsable Inscripto"
        elif cat_iva == 2:
            response = "IVA Responsable no Inscripto"
        elif cat_iva == 3:
            response = "IVA no Responsable"
        elif cat_iva == 4:
            response = "IVA Sujeto Exento"
        elif cat_iva == 5:
            response = "Consumidor Final"
        elif cat_iva == 6:
            response = "Responsable Monotributo"
        elif cat_iva == 7:
            response = "Sujeto no Categorizado"
        elif cat_iva == 8:
            response = "Proveedor del Exterior"
        elif cat_iva == 9:
            response = "Cliente del Exterior"
        elif cat_iva == 10:
            response = "IVA Liberado – Ley Nº 19.640"
        elif cat_iva == 11:
            response = "IVA Responsable Inscripto – Agente de Percepción"
        elif cat_iva == 12:
            response = "Pequeño Contribuyente Eventual"
        elif cat_iva == 13:
            response = "Monotributista Social"
        elif cat_iva == 14:
            response = "Pequeño Contribuyente Eventual Social"
        else:
            response = ""

        return response

    @api.multi
    def update_aux_controlafip_from_api_afip(self):
        aux_controlafip = self.search([])
        for index, row in enumerate(aux_controlafip):
            persona_encontrada = self.env['afip.service'].search(
                    [], limit=1).consulta_afip_id(row.numerocuit_jsat)

            if persona_encontrada:
                print('-'*80)
                print('{0} - PERSONA ENCONTRADA:{1}'.format(index, row.numerocuit_jsat))
                print('-'*80)

                estadoClave_AFIP = persona_encontrada.get("estado")
                nombreCliente_AFIP = persona_encontrada.get(
                        "denominacion").replace("'", "''").strip()
                codigoCondicionIVA_AFIP = persona_encontrada.get("cat_iva")
                condicionIVA_AFIP = self._categoria_iva(codigoCondicionIVA_AFIP)
                tipoPersona_AFIP = persona_encontrada.get("tipo_persona")
                monotributo_AFIP = persona_encontrada.get("monotributo")
                actividadMonotributo_AFIP = persona_encontrada.get(
                        "actividad_monotributo")
                success = "Encontrada"

                try:
                    print(row)
                    row.estadoclave_afip = estadoClave_AFIP
                    row.nombrecliente_afip = nombreCliente_AFIP
                    row.codigocondicioniva_afip = codigoCondicionIVA_AFIP
                    row.condicioniva_afip = condicionIVA_AFIP
                    row.tipopersona_afip = tipoPersona_AFIP
                    row.monotributo_afip = monotributo_AFIP
                    row.actividadmonotributo_afip = actividadMonotributo_AFIP
                    row.success = success
                except:
                    print('Error al actualizar model aux_controlAFIP')
                    continue
            else:
                print('-'*80)
                print("NO ENCONTRADO - {0}".format(index))
                print('-'*80)

                row.success = 'NO ENCONTRADA'

    # @api.multi
    def upload_records_from_aux_controlafip_sqlserver(self):
        self.search([]).unlink()

        rows_personas = []
        db = self.env['afip.dbconection'].search([
            ('active', '=', 'True'),
            ('host', 'ilike', 'pandora'),
            ('database', 'ilike', 'ctsma')], limit=1)

        try:
            conn = pymssql.connect(db.host, db.user, db.password, db.database)
            cursor = conn.cursor()
            cursor.execute("SELECT * from aux_ControlAFIP")
            rows_personas = cursor.fetchall()
        except Exception as error:
            print('Error en upload_records_from_aux ', error)

        for row in rows_personas:
            row_insert = {'idpersona_jsat': row[0],
                    'numerocliente_jsat': row[1],
                    'tipopersona_jsat': row[2],
                    'nombrecliente_jsat': row[3],
                    'numerocuit_jsat': row[4],
                    'idcondicioniva_jsat': row[5],
                    'codigocondicioniva_jsat': row[6],
                    'condicioniva_jsat': row[7],
                    'estadoclave_afip': row[8],
                    'nombrecliente_afip': row[9],
                    'codigocondicioniva_afip': row[10],
                    'condicioniva_afip': row[11],
                    'tipopersona_afip': row[12],
                    'monotributo_afip': row[13],
                    'actividadmonotributo_afip': row[14],
                    'success': row[15],
                    'fechaproceso': row[16]}
            try:
                self.create(row_insert)
            except Exception as error:
                print('Error al crear registro', error)
                continue

    # Este job ejecuta la actulizacion para el cotesma net original
    #
    # @api.multi
    def ejecutar_job(self):
        print('Ejecutando Store Proc')
        self.ejecutar_store_proc()

        self.with_delay().cotesmanet_afip_update()
        return True

    # Esto ejecuta el job para actualizar el model de Odoo
    @job
    def ejecutar_job_local(self):
        print('Ejecutando Store Proc')
        self.ejecutar_store_proc()

        print('Cargando registros desde sql server a Odoo')
        self.upload_records_from_aux_controlafip_sqlserver()

        print('Actualizar los registro del model Odoo con la info de la afip')
        self.update_aux_controlafip_from_api_afip()
        return True

    @api.multi
    def ejecutar_job_local_solo_fin_de_mes(self):
        dos_dias_antes_de_fin_de_mes = calendar.monthrange(
                datetime.now().year, datetime.now().month)[1] - 2

        if datetime.now().day == dos_dias_antes_de_fin_de_mes:
            # Ejecuto el job local para el model de Odoo
            self.with_delay().ejecutar_job_local()

            # Ejecuto el Job remoto para el cotesma net original
            self.ejecutar_job()
        else:
            print('No es fin de mes hoy')
            # self.with_delay().ejecutar_job_local()

class ConsultaPadronOnline(models.TransientModel):
    _name = 'afip.consultapadrononline'
    _description = 'Consulta Padron Online'
    _rec_name = 'cuit'

    cuit = fields.Char()
    denominacion = fields.Char()
    tipo_persona = fields.Char()
    tipo_doc = fields.Char()
    dni = fields.Char()
    estado = fields.Char()
    direccion = fields.Char()
    localidad = fields.Char()
    provincia = fields.Char()
    cod_postal = fields.Char()
    impuestos = fields.Char()
    actividades = fields.Char()
    imp_iva = fields.Char()
    cat_iva = fields.Char()
    monotributo = fields.Char()
    actividad_monotributo = fields.Char()
    empleador = fields.Char()

    @api.one
    def find_cuit(self):
        if self.cuit:
            pa5 = self.env.get('afip.service').search([('alias_id.alias', '=', 'ws_sr_padron_a5')], limit=1)
            ok = pa5.consulta_afip_id(self.cuit)
            if ok:
                self.denominacion = ok.get('denominacion')
                self.cuit = ok.get('cuit')
                self.tipo_persona = ok.get('tipo_persona')
                self.tipo_doc = ok.get('tipo_doc')
                self.dni = ok.get('dni')
                self.estado = ok.get('estado')
                self.direccion = ok.get('direccion')
                self.localidad = ok.get('localidad')
                self.provincia = ok.get('provincia')
                self.cod_postal = ok.get('cod_postal')
                self.impuestos = ok.get('impuestos')
                self.actividades = ok.get('actividades')
                self.imp_iva = ok.get('imp_iva')
                self.cat_iva = ok.get('cat_iva')
                self.monotributo = ok.get('monotributo')
                self.actividad_monotributo = ok.get('actividad_monotributo')
                self.empleador = ok.get('empleador')
            else:
                self.denominacion = ''
                self.tipo_persona = ''
                self.tipo_doc = ''
                self.dni = ''
                self.estado = ''
                self.direccion = ''
                self.localidad = ''
                self.provincia = ''
                self.cod_postal = ''
                self.impuestos = ''
                self.actividades = ''
                self.imp_iva = ''
                self.cat_iva = ''
                self.monotributo = ''
                self.actividad_monotributo = ''
                self.empleador = ''
                # raise ValidationError('Cuit not found')

    @api.constrains("cuit")
    def _validate_cuit(self):
        if not self.cuit:
            raise ValidationError('Cuit can not be empty')

    @api.multi
    def do_count_tasks(self):
        raise ValidationError('do_coubt_task!')

    @api.multi
    def do_populate_tasks(self):
        raise ValidationError('do_populate_tasks!')

    @api.multi
    def do_mass_update(self):
        # raise ValidationError('do_mass_update!')
        # self.ensure_one()
        self.denominacion = 'FoxCarlos'

class MessageBox(models.TransientModel):
    _name = 'afip.messagebox'
    _description = 'MessageBox Popup'
    _rec_name = 'text'

    text = fields.Text()

    @api.multi
    def delete_messages(self):
        for record in self:
            record.unlink()

    @api.multi
    def _reopen_form(self):
        self.ensure_one()
        action = {
            'type': 'ir.actions.act_window',
            'res_model': self._name,
            'res_id': self.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }
        return action

    @api.multi
    def do_populate_tasks(self):
        self.ensure_one()
        conexion = self.env['afip.dbconection'].search([], limit=1)
        ok, msg = conexion.test_conection()[0]

        if ok:
            self.text = '<p><b><font style="font-size: 14px;" class="text-alpha">{0}</font></b></p>'.format(msg)
        else:
            self.text = '<p><b><font style="font-size: 14px;" class="text-gamma">{0}</font></b></p>'.format(msg)

        return self._reopen_form()
