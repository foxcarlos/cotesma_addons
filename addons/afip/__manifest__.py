# -*- coding: utf-8 -*-
{
    'name': "afip",

    'summary': """
        Consultas de personas a la AFIP""",

    'description': """
        Modulo para realizar consultas de personas a la AFIP""",

    'author': "Carlos Garcia",
    'website': "http://cotesma.com.ar",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'HR',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','queue_job'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views_menu.xml',
        'views/views.xml',
        'views/views_messagebox.xml',
        'views/templates.xml',
        'data/afip.dbconection.xml',
        'data/afip.alias.xml',
        'data/afip.service.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
