# -*- coding: utf-8 -*-

# 1 : imports of python lib
# 2 : imports of odoo
# 3 : imports from odoo modules

# import unicodedata
import json

from odoo import http


class Intranet(http.Controller):
    '''.'''
    @http.route('/', auth='public', website=True)
    def index2(self, **kw):
        '''.'''
        return "Hola, Mundo"

    @http.route('/afip/', auth='public', website=True)
    def index(self, **kw):
        '''.'''
        return "Hola, Mundo"

    @http.route('/afip/<name>/', auth='none', website=True, cors='*')
    def nombre(self, name):
        '''.'''
        # return '<h1>El nombre pasado es:{0}</h1>'.format(name)
        x = {'id': 1, 'name': 'telefono', 'cost': 4000, 'quantity': 2}
        return json.dumps(x)

    def _validate_jobs(self, http):
        return http.request.env['queue.job'].sudo().search([
            ('state', '!=', 'done'),
            ('method_name', '=', 'cotesmanet_afip_update'),
            ])

    @http.route('/afip/padron/cotesmanet/validate/auxcontrolafip/', auth='public', website=True)
    def validate_cotesmanet(self, **kw):
        return 'Ya existe un trabajo en ejecucion' \
                if self._validate_jobs(http) else 'ok'

    @http.route('/afip/padron/cotesmanet/update/auxcontrolafip/', auth='public', website=True)
    def cotesmanet(self, **kw):

        trabajos_en_ejecucion = self._validate_jobs(http)
        if trabajos_en_ejecucion:
            return 'Ya existe un trabajo en ejecucion'
        else:
            http.request.env['afip.aux_controlafip'].sudo().with_delay().cotesmanet_afip_update()
            return 'ok'

    @http.route('/afip/padron/consulta/<int:id>/', auth='none', website=True)
    def id2(self, id):
        response = ''
        servicio = http.request.env['afip.service'].sudo().search([
            ('alias_id', '=', 'ws_sr_padron_a5'),
            ('alias_id.homo', '=', False),
            ('alias_id.active', '=', True)])

        response = servicio.consulta_afip_id(id)
        if response:
            response = json.dumps(response[0])
        return response

    # Consulta por  Cuit y por Servicio
    @http.route('/afip/<servicio>/consulta/<int:cuit>/', auth='none', website=True)
    def consulta_servicio_cuit(self, servicio, cuit):
        response = ''
        service = http.request.env['afip.service'].sudo().search([
            ('servicio', '=', servicio), ('alias_id.active', '=', True)],
            limit=1)

        response = service.consulta_afip_id(cuit)
        if response:
            response = json.dumps(response[0])
        return response
