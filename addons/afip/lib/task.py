# !/usr/bin/env python2.7
# -*- coding: latin-1 -*-
from time import sleep

import pymssql
from datetime import datetime

from odoo import api, modules
from odoo.tools.config import config


def get_params_dbconection():
    response = []
    db_name = config.get('db_name')
    record = modules.registry.RegistryManager.get(db_name)
    cr = record.cursor()
    env = api.Environment(cr, 1, {})
    domain = [
            ('active', '=', True),
            ('host' , 'ilike', 'pandora'),
            ('database', 'ilike', 'ctsma')]

    db = env['afip.dbconection'].search(domain, limit=1)

    if db:
        response = db.host, db.user, db.password, db.database
    return response

def actualizar_registros(servicio):
    def categoria_iva(cat_iva):
        if cat_iva == 1:
            response = "IVA Responsable Inscripto"
        elif cat_iva == 2:
            response = "IVA Responsable no Inscripto"
        elif cat_iva == 3:
            response = "IVA no Responsable"
        elif cat_iva == 4:
            response = "IVA Sujeto Exento"
        elif cat_iva == 5:
            response = "Consumidor Final"
        elif cat_iva == 6:
            response = "Responsable Monotributo"
        elif cat_iva == 7:
            response = "Sujeto no Categorizado"
        elif cat_iva == 8:
            response = "Proveedor del Exterior"
        elif cat_iva == 9:
            response = "Cliente del Exterior"
        elif cat_iva == 10:
            response = "IVA Liberado – Ley Nº 19.640"
        elif cat_iva == 11:
            response = "IVA Responsable Inscripto – Agente de Percepción"
        elif cat_iva == 12:
            response = "Pequeño Contribuyente Eventual"
        elif cat_iva == 13:
            response = "Monotributista Social"
        elif cat_iva == 14:
            response = "Pequeño Contribuyente Eventual Social"
        else:
            response = ""

        return response

    def consulta_interna(cuit):
        response = []
        response = servicio.consulta_afip_id(cuit)
        sleep(2)
        if response:
            response = response[0] if type(response) == 'list' else response
        return response

    rows_personas = []
    # servidor = 'fobos'
    conectar = get_params_dbconection()

    HOST, USER, PASSWORD, DB = conectar if conectar else ['', '', '', '']

    conn = pymssql.connect(HOST, USER, PASSWORD, DB)
    cursor = conn.cursor()
    cursor.execute("SELECT numeroCuit_JSAT from aux_ControlAFIP where success = '' ")
    rows_personas = cursor.fetchall()

    fecha_inicial = datetime.now()
    for index, persona_consultar in enumerate(rows_personas):
        persona_encontrada = consulta_interna(persona_consultar[0])
        if persona_encontrada:
            print('-'*80)
            print('{0} - PERSONA ENCONTRADA:{1}'.format(index, persona_consultar[0]))
            print('-'*80)

            cuit = '{0}'.format(persona_consultar[0])
            estadoClave_AFIP = persona_encontrada.get("estado")
            nombreCliente_AFIP = persona_encontrada.get(
                    "denominacion").replace("'", "''").strip()
            codigoCondicionIVA_AFIP = persona_encontrada.get("cat_iva")
            condicionIVA_AFIP = categoria_iva(codigoCondicionIVA_AFIP)
            tipoPersona_AFIP = persona_encontrada.get("tipo_persona")
            monotributo_AFIP = persona_encontrada.get("monotributo")
            actividadMonotributo_AFIP = persona_encontrada.get(
                    "actividad_monotributo")
            success = "Encontrada"

            sql_update = """update aux_ControlAFIP set success='{0}',
            estadoClave_AFIP='{1}',
            nombreCliente_AFIP='{2}',
            condicionIVA_AFIP='{3}',
            tipoPersona_AFIP='{4}',
            monotributo_AFIP='{5}',
            actividadMonotributo_AFIP='{6}',
            codigoCondicionIVA_AFIP='{7}'
            where numeroCuit_JSAT = '{8}' """.format(
                    success,
                    estadoClave_AFIP,
                    nombreCliente_AFIP,
                    condicionIVA_AFIP,
                    tipoPersona_AFIP,
                    monotributo_AFIP,
                    actividadMonotributo_AFIP,
                    codigoCondicionIVA_AFIP,
                    cuit,
                    )

            cursor.execute(sql_update)
            conn.commit()
        else:
            print('-'*80)
            print("NO ENCONTRADO - {0}".format(index))
            print('-'*80)

            success = "No Encontrada"

            sql_update = """update aux_ControlAFIP set success='{0}'
            where  numeroCuit_JSAT = '{1}'""".format(
                    success, persona_consultar[0])

            cursor.execute(sql_update)

    fecha_final = datetime.now()
    minutos_transcurridos = (fecha_final - fecha_inicial)
    print(minutos_transcurridos)
