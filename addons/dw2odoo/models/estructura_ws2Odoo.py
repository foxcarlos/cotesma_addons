# -*- coding: utf-8 -*-
import pyodbc

"""
conn = pyodbc.connect(
    ...: driver="/usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so",
    ...: TDS_Version='7.0',
    ...: server="192.168.1.28",
    ...: port="1433",
    ...: database="jsatsmandes",
    ...: uid="ctsma",
    ...: pwd="PeterPan"w)
"""
# sql = "SELECT *FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Tablero_AccesosVelocidad'"

# :class:TransientModel

import ipdb; ipdb.set_trace() # BREAKPOINT
conn = pyodbc.connect(driver="/usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so",
        TDS_Version='7.0',
        server="192.168.1.21",
        port="1433",
        database="dw",
        uid="dw",
        pwd="D4t4wh0use")


cursor = conn.cursor()
sql_tecnica = "SELECT *FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'Tecnica_ReclamosBalance'"
sql_tecnica = "SELECT a\xf1o, nombre, codigo, mes, fecha, dia, diadelasemana, servicio, pendientes, nuevos, resueltos, balance, tendencia  FROM Tecnica_ReclamosBalance"

cursor.execute(sql_tecnica)
rows = cursor.fetchall()

for fila in rows:
    _,_,_,nombre,_,_,_,tipo,long,_,_,_,_,_,_,_,_,_,_,_,_,_,_ = fila
    print(nombre, tipo, long)

"""
(u'a\xf1o', u'int', None)
(u'nombre', u'varchar', 40)
(u'codigo', u'char', 6)
(u'mes', u'varchar', 40)
(u'fecha', u'datetime', None)
(u'dia', u'varchar', 2)
(u'diaDeLaSemana', u'varchar', 9)
(u'servicio', u'varchar', 50)
(u'pendientes', u'int', None)
(u'nuevos', u'int', None)
(u'resueltos', u'int', None)
(u'balance', u'int', None)
(u'tendencia', u'varchar', 9)
"""
