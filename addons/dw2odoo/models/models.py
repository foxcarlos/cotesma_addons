# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

# class dw2odoo(models.Model):
#     _name = 'dw2odoo.dw2odoo'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100


class DailyBalance(models.Model):
    _name = 'dw2odoo.dailybalance'
    _description = 'Balance of Daily Issues'
    _rec_name = 'year'
    # _auto = False

    year = fields.Integer(string='Year')
    name = fields.Char(string='Name', size=40)
    code = fields.Char(string='Code', size= 6)
    month = fields.Char(string='Month', size=40)
    date = fields.Date(string='date')
    day =  fields.Integer(string='Day')
    weekday = fields.Char(string='WeekDay', size=9)
    service =  fields.Char(string='Service', size=50)
    pending =  fields.Integer(string='Pending')
    new =  fields.Integer(string='New')
    done =  fields.Integer(string='Done')
    balance =  fields.Integer(string='Balance')
    trend =  fields.Char(string='Trend', size=9)


    def importa(self):
        cabecera = ['nombre', 'codigo', 'mes', 'fecha', 'dia',
                'diaDeLaSemana', 'servicio', 'pendientes',
                'nuevos', 'resueltos', 'balance', 'tendencia']

        detalle = (u'ENERO 2018', u'201801', u'ENERO',
                datetime.datetime(2018, 1, 31, 0, 0), u'31', u'Miercoles',
                u'INTERNET REGIONAL', 58, 4, 3, 59, u'A LA ALTA')

        row_data = dict(zip(cabecera, detalle))
        BalanceDiario = self.env['dw2odoo.dailybalance']
        new = BalanceDiario.create(row_data)

