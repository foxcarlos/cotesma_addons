# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError, UserError

# class registro_herencia(models.Model):
#     _name = 'registro_herencia.registro_herencia'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class personas(models.Model):
    _inherit = 'persona'
    _description = 'Personas Reload'

    numero_hijos = fields.Integer(string='N hjos',
                          help='Ingrese la cantidad de hijos que posee')

    @api.constrains("numero_hijos")
    def _valida_nhijos(self):
        if self.numero_hijos and not self.fecha_nacimiento:
            raise ValidationError('Es necesario que tenga fecha de nacimiento\
                    para poder ingresar numero de hijos')
