#-*- coding:utf-8 -*-

from odoo.tests.common import TransactionCase

class TestPersona(TransactionCase):
    def test_create(self):
        "Create a simple test"
        Persona = self.env['mimodulo.persona']
        insertar = Persona.create({'dni': '32582611'})
        
        # self.assertEqual(insertar.active, True)
        self.assertEqual(insertar.active, 'a')
