# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import re
from odoo import api,fields,models
from odoo.exceptions import ValidationError, UserError



class Persona(models.Model):
	"""Model para el registro de personas"""
	_description = 'Models de personas'
	_name = 'veoc.persona'
	_rec_name = 'nombre'
	_order = 'nombre desc'

	nombre =fields.Char(string='Nombre', help='Ingrese su nombre',required=True)
	apellido = fields.Char(string='Apellidos', help='Ingrese su apellido',required=True)
	fecha_nacimiento = fields.Date(string='Fecha de nacimiento',help='Ingrese su fecha de nacimiento con el siguiente formato DD/MM/AAAA',required=True)
	
#@api.one es para un metodo que se crea para un campo en el que estoy posicionado
	@api.one
	def mimetodo(self):
		pass

	@api.constrains("fecha_nacimiento")
	def _validate_fecha_nacimiento(self):
		hoy = datetime.now()
		mayor = hoy - timedelta(years=18)
		if self.fecha_nacimiento > mayor:
			raise ValidationError(
				'La fecha de nacimiento "{0}" no corresponde a una persona mayor de 18 anos'.format(self.fecha_nacimiento)
			)

	#@api.multi
	#def name_get(self):
	#	result=[]
	#	for persona in self:
	#		name="{0} - {1} - {2}".format(persona.dni,persona.nombres,persona.apellidos)
	#		result.append(name)
	#	return result


#@api.multi es para aplicar un metodo a varios registros a la vez en lugar de uno

#@api.constrains es para hacer validaciones antes de que odoo haga el save. Estos metodos deben ser privados y eso se determina con un _antes del nombre


class Cliente(models.Model):
	"""Model para el registro de clientes"""
	_description = 'Models de clientes'
	_name = 'veoc.cliente'
	_rec_name = 'mail'
	_order = 'mail asc'

	mail =fields.Char(string='Mail', help='Ingrese su mail',required=True)
	contrasena =fields.Char(string='Contrasena', help='Ingrese una contrasena. Debe cumplir con las siguientes reglas: Incluir números, Incluir letras mayúsculas y minúsculas, Mínimo 8 caracteres',required=True)
	confirm_contrasena = fields.Char(string='Confirmar Contrasena',required=True)
	localidad_id = fields.Many2one('veoc.localidad')
	persona_id = fields.Many2one('veoc.persona')
	provincia_id = fields.Many2one('veoc.provincia')
	admite_newsletter = fields.Boolean(string='Newsletter',default=False, help='Permitir newsletter')
	_sql_constraints = [('mail_unico','UNIQUE(mail)','El mail ingresado ya existe')]
	_sql_constraints = [('persona_id_unico','UNIQUE(persona_id)','Ya existe la persona')]

	#valido el formato del mail
	@api.constrains("mail")
	def _validate_mail(self):
		if not re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',self.mail.lower()):
			raise ValidationError(
				'El formato del mail "{0}" es incorrecto'.format(self.mail)
			)
	
	#valido que la contrasena contenga al menos 8 caracteres, que contenga números y letras
	# y que contenga mayúsculas y minúsculas.
	""" @api.constrains("contrasena")
	def _validate_contrasena(self):
		if len(self.contrasena) < 8:
			raise ValidationError('La contrasena debe contener al menos 8 caracteres') 
		
		 regex = re.compile(r'[0-9]+[(a-z)]+[(A-Z)]')

		if not regex.search(self.contrasena):
			raise ValidationError('El formato de la contrasena "{0}" es incorrecto'.format(self.contrasena)) """


class Localidad(models.Model):
	"""Model para el registro de localidades"""
	_description = 'Models de localidad'
	_name = 'veoc.localidad'
	_rec_name = 'descripcion'
	_order = 'descripcion asc'

	descripcion =fields.Char(string='Descripción', help='Ingrese el nombre de la localidad',required=True)
	provincia_id = fields.Many2one('veoc.provincia')
	_sql_constraints = [('descripcion_unico','UNIQUE(descripcion)','La localidad ingresada ya existe')]


class Provincia(models.Model):
	"""Model para el registro de provincias"""
	_description = 'Models de provincia'
	_name = 'veoc.provincia'
	_rec_name = 'descripcion'
	_order = 'descripcion asc'

	descripcion =fields.Char(string='Descripción', help='Ingrese el nombre de la provincia', required=True)
	_sql_constraints = [('descripcion_unico','UNIQUE(descripcion)','La provincia ingresada ya existe')]

class CentroCultural(models.Model):
	"""Model para el registro del centro cultural"""
	_description = 'Models del Centro Cultural'
	_name = 'veoc.centrocultural'
	_rec_name = 'nombre'
	_order = 'nombre asc'

	nombre =fields.Char(string='Nombre', help='Ingrese el nombre del centro cultural',required=True)
	direccion = fields.Text(string='Direccion', help='Ingrese la direccion',required=True)
	_sql_constraints = [('nombre_unico','UNIQUE(nombre)','El centro cultural ingresado ya existe')]

class Sala(models.Model):
	"""Model para el registro de las salas"""
	_description = 'Models de salas'
	_name = 'veoc.sala'
	_rec_name = 'nombre'
	_order = 'nombre asc'

	codigo = fields.Char(string='Código', size = 4 , help='Ingrese el código de la sala',required=True)
	nombre =fields.Char(string='Nombre', help='Ingrese el nombre de la sala', required=True)
	capacidad_maxima = fields.Integer(string='Capacidad Máxima', size=4, default = 0, help='Capacidad máxima de asientos que admite la sala', required=True)
	butaca_ids = fields.One2many ('veoc.butaca', 'sala_id', string="Butacas")
	#evento_ids = fields.One2many ('veoc.evento', 'sala_id', string="Eventos")
	mapa_sala=fields.Binary(string='Mapa de la sala', help='Disposición de la sala')
	_sql_constraints = [('codigo_unico','UNIQUE(codigo)','El código ya existe')]
	_sql_constraints = [('nombre_unico','UNIQUE(nombre)','La sala ingresada ya existe')]

class Butaca(models.Model):
	"""Model para el registro de las butacas"""
	_description = 'Models de butacas'
	_name = 'veoc.butaca'
	_rec_name = 'nro'
	_order = 'nro asc'

	nro =fields.Char(string='Número de butaca', size = 3, help='Ingrese el número de la butaca',required=True)
	sector_id = fields.Many2one ('veoc.sector')
	sala_id = fields.Many2one ('veoc.sala')
	_sql_constraints = [('nro_unico','UNIQUE(nro)','El butaca ya existe')]


class Sector(models.Model):
	"""Model para el registro de sectores de salas"""
	_description = 'Models de sectores'
	_name = 'veoc.sector'
	_rec_name = 'sector'
	_order = 'codigo asc'

	codigo =fields.Char(string='Código', size = 3, help='Ingrese el código del sector',required=True)
	sector = fields.Char(string='Sector', help='Ingrese el sector', required=True)
	_sql_constraints = [('codigo_unico','UNIQUE(codigo)','El sector ya existe')]

class TipoEvento(models.Model):
	"""Model para el registro de los tipos de eventos"""
	_description = 'Models de tipos de eventos'
	_name = 'veoc.tipoevento'
	_rec_name = 'descripcion'
	_order = 'codigo asc'

	codigo =fields.Integer(string='Código', size = 3, help='Ingrese el código del tipo de evento',required=True)
	descripcion =fields.Char(string='Descripcion', help='Ingrese la descripción del tipo de evento', required=True)
	#evento_ids = fields.One2many ('veoc.evento', 'tipoevento_id', string="Eventos")
	_sql_constraints = [('codigo_unico','UNIQUE(codigo)','El tipo de evento ya existe')]

class Tarjeta(models.Model):
	"""Model para el registro de tarjetas como medio de pago"""
	_description = 'Models de tarjetas'
	_name = 'veoc.tarjeta'
	_rec_name = 'numero'
	_order = 'numero asc'

	numero = fields.Integer(string='Número de Tarjeta', size = 16, help='Ingrese el número de la tarjeta',required=True)
	titular = fields.Text (string= 'Titular', help='Ingrese el nombre del titular de la tarjeta tal cual aparece en la tarjeta', required=True)
	vencimiento = fields.Char(string='Fecha de vencimiento', help='Ingrese la fecha de vencimiento con el formato MM/AA', required=True)
	cod_seguridad = fields.Integer(string='Código de Seguridad', size = 3, help='Ingrese el código de seguridad',required=True)
	entidad = fields.Selection(string='Entidad', help='Ingrese la entidad de la tarjeta', required=True, selection=[(1,'VISA'), (2,'MASTERCARD'),(3,'AMERICAN EXPRESS'), (4,'DINERS'), (5,'CREDIGUIA')])
	categoria = fields.Selection(string='Categoría', help='Ingrese la categoría', required=True, selection=[(1,'Débito'), (2,'Crédito')])
	banco = fields.Selection(string='Banco', help='Ingrese el banco de la tarjeta', required=True,selection=[(1,'BBVA Francés'), (2,'Naranja'), (3,'Galicia'), (4,'Nación'), (5,'Provincia del Neuquén'), (6,'Provincia de Bs.As.'), (7,'Patagonia'), (8,'Santander'), (9,'ICBC'), (10,'Macro')])
	_sql_constraints = [('numero_unico','UNIQUE(numero)','La tarjeta ya existe')]

#hacer metodo para que complete la barra en el campo vencimiento

class Evento(models.Model):
	"""Model para el registro de eventos"""
	_description = 'Models de eventos'
	_name = 'veoc.evento'
	_rec_name = 'descripcion'
	_order = 'descripcion asc'	

	codigo =fields.Integer(string='Código', size = 3, help='Ingrese el código del evento',required=True)
	descripcion =fields.Char(string='Descripcion', help='Ingrese la descripción del evento', required=True)
	fecha_hora_inicio = fields.Datetime(string='Fecha y Hora inicio',help='Fecha y Hora del evento',required=True)
	fecha_hora_fin = fields.Datetime(string='Fecha y Hora fin',help='Fecha y Hora del evento',required=True)
	tipoevento_id = fields.Many2one ('veoc.tipoevento')
	clasificacion_id = fields.Many2one('veoc.clasificacion')
	foto = fields.Binary(string='Gráfica', help='Seleccione una foto')
	website = fields.Char(string='Website', help='Website del evento')
	entrada_ids=fields.One2many ('veoc.entrada', 'evento_id', string="Entradas")
	sala_id = fields.Many2one('veoc.sala')
	duracion = fields.Float (string='Duración del evento', help='Duración del evento')
	precio_ids=fields.One2many ('veoc.precioevento', 'evento_id', string="Precio por Sector")
	funcion_ids=fields.One2many ('veoc.funcion', 'evento_id', string="Funciones")
	_sql_constraints = [('codigo_unico','UNIQUE(codigo)','El código ya existe')]
	_sql_constraints = [('descripcion_unico','UNIQUE(descripcion)','El evento ya existe')]

	#se calcula la duración del evento en base a la fecha y hora de inicio y fecha y hora de finalización
	"""@api.depends('fecha_hora_fin','fecha_hora_inicio')
    def _duracion(self):
        for r in self: 
		 r.duracion = r.fecha_hora_fin - r.fecha_hora_inicio"""


class Clasificacion(models.Model):
	"""Model para el registro clasificaciones"""
	_description = 'Models de clasificación para eventos'
	_name = 'veoc.clasificacion'
	_rec_name = 'descripcion'
	_order = 'codigo asc'	

	codigo = fields.Char(string='Código', size = 4 , help='Ingrese el código de la clasificación',required=True)
	descripcion =fields.Char(string='Descripcion', help='Ingrese una descripción', required=True)
	_sql_constraints = [('codigo_unico','UNIQUE(codigo)','El código ya existe')]

class PrecioEvento (models.Model):
	"""Model para el registro de la lista de precios de cada evento"""
	_description = 'Models de lista de precios de los eventos'
	_name = 'veoc.precioevento'
	_rec_name = 'id'
	_order = 'id asc'	

	evento_id = fields.Many2one('veoc.evento', required=True)
	sector_id = fields.Many2one('veoc.sector',required=True)
	precio = fields.Float (string='Precio Unitario', help='Ingrese el precio', required = True)


class Funcion(models.Model):
	"""Model para el registro de las funciones de cada evento"""
	_description = 'Models de funciones'
	_name = 'veoc.funcion'
	_rec_name = 'id'
	_order = 'id asc'	

	evento_id = fields.Many2one('veoc.evento', required=True)
	fecha_hora = fields.Datetime(string='Fecha de la función')


class Entrada(models.Model):
	"""Model para el registro de entradas"""
	_description = 'Models de entradas'
	_name = 'veoc.entrada'
	_rec_name = 'id'
	_order = 'id asc'	

	evento_id = fields.Many2one ('veoc.evento', required=True)
	precio = fields.Float (string='Precio', help='Precio de la entrada')
	butaca_id = fields.Many2one ('veoc.butaca')
	numero = fields.Integer (string='Número', help='Número de entrada') #se completa solo y es autoincremetal
	comprobante_id = fields.Many2one ('veoc.comprobante')
	fecha_funcion = fields.Datetime (string = 'Fecha', help='Fecha de la función', required=True)
	esINCAA = fields.Boolean (string = 'INCAA', default=False, help='Informa si la película es INCAA')
	menores = fields.Boolean (string = 'Asisten menores?', default=False, help='Informar si van a asistir personas menores a la clasificación del evento')
	#mensaje_menores = fields.Text (string = '')

	#chequeo si el evento es INCAA
	@api.multi
	def esINCAA(self):
		if evento_id.tipo == "Cine Nacional":
			esINCAA=True
		return esINCAA
	
	#se calcula el precio de la entrada en fase a la selección del asiento
	


class Comprobante(models.Model):
	"""Model para el registro de comprobante"""
	_description = 'Models de comprobantes'
	_name = 'veoc.comprobante'
	_rec_name = 'numero'
	_order = 'numero asc'	

	numero = fields.Integer (string='Número de comprobante', help='Número de comprobante') #se completa solo y es autoincremetal
	cliente_id = fields.Many2one ('veoc.cliente')
	entrada_ids = fields.One2many ('veoc.entrada', 'comprobante_id', string="Entradas")
	cantidad_entradas = fields.Integer (string = 'Cantidad total de entradas')
	cantidad_entradas_por_sector = fields.Integer (string = 'Cantidad de entradas por sector')
	monto_por_sector = fields.Float (string = 'Monto por sector')
	monto_total = fields.Float (string = 'Monto Total')
	_sql_constraints = [('numero_unico','UNIQUE(numero)','El comprobante ya existe')]

	
