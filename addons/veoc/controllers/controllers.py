from odoo import http

#es para utilizarlo para cualquier consulta web

class CentroCultural(http.Controller):
    @http.route('/centroculturalcotesma/', auth='public', website=True)
    def index(self, **kw):
        #Buscar en los modelos
        eventos = http.request.env['veoc.evento']
        params = {"Eventos":eventos.search([])}
        return http.request.render('veoc.index',params)

    @http.route('/centro/', auth='public', website=True)
    def centro(self, **kw):
        return http.request.render('veoc.centro')

    @http.route('/test/', auth='public', website=True)
    def test(self, **kw):
        return http.request.render('veoc.test')
        
"""     @http.route('/centroculturalcotesma/eventos/', auth='public', website=True)
    def eventos(self,nombres):
        params = {'persona':nombres}
        return http.request.render('veoc.biografia',params)
 """