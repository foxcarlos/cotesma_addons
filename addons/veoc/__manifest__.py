

{
    'name': "veoc",

    'summary': """
        Venta de entradas online para COTESMA""",

    'description': """
        Mediante esta app se puede realizar la compra de entradas 
        para los eventos del Centro Cultural Cotesma, de manera online""",

    'author': "Rocío Gómez Mon",
    'website': "http://cotesma.com.ar",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Desconocida',
    'version': '0.1',
    'installable': True,

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': ['security/ir.model.access.csv','views/form_views.xml','views/tree_views.xml', 'views/search_views.xml', 
         'views/menus.xml', 'views/templates.xml'],
         #'security/ir.model.access.csv'
    # only loaded in demonstration mode

}
