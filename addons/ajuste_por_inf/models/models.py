# -*- coding: utf-8 -*-
import pymssql

from odoo import models, fields, api


class Indices(models.Model):
    _name = 'ajuste_por_inf.indices'

    year = fields.Integer(string='Año')
    month = fields.Integer(string='Mes')
    index = fields.Float(string='Indice', digits=(6, 4), default=0.0)

    @api.model
    def insertar_cuentas(self):
        recordset = self.env['ajuste_por_inf.cuentas'].search([], limit=1)
        recordset.insert_counts()

    @api.model
    def preparar_ajuste_por_inf(self):
        recordset = self.env['account.ajuste_por_inf'].search([], limit=1)
        recordset.insert_rows_empty()

class CuentasCierreAperturaEjercicio(models.TransientModel):
    _name = 'ajuste_por_inf.cuentacierreapertura'
    _description = 'Cuenta Cierre y Apertura de Ejercicio'
    _order = 'count_code, year, month'

    count_code = fields.Char(string='Codigo Cuenta')
    descripcion = fields.Char(string='Descripcion de Cuenta')
    debito = fields.Float(string='Debito')
    credito = fields.Float(string='Credito')
    year = fields.Integer(string='Año')
    month = fields.Integer(string='Mes')
    tipo_comprob = fields.Char(string='Tipo Comprobante')
    balance = fields.Float(string='Saldo', default=0.0)

    def borrar_cuentas(self):
        recordset = self.env['ajuste_por_inf.cuentacierreapertura'].search([])
        recordset.unlink()

    def import_from_sql_server_cuenta_cierre_y_apertura_de_ejercicio(self):
        server_params = self.env['ajuste_por_inf.cuentas']

        sql_cad = """SELECT la.codigo_cta, c.descrip_cta , la.debito, la.credito,  YEAR(a.fecha_asiento) as ANIO,   MONTH(a.fecha_asiento) as MES,a.tipo_comprob
		    FROM SMAndes.dbo.Lineas_asiento la
		    inner join SMAndes.dbo.Asientos a (nolock) on la.nro_asiento = a.nro_asiento
                    inner join SMAndes.dbo.cuentas c (nolock) on la.codigo_cta = c.codigo_cta
		    where (a.tipo_comprob = 'CPEJ') and
		    (la.codigo_cta like '1.2.03.04' or
		    la.codigo_cta like '1.2.01.%' or la.codigo_cta like '1.2.02.%' or
		    la.codigo_cta like '1.2.03.04' or la.codigo_cta like '3.1.01.%' or
		    la.codigo_cta like '3.2.01.%' or
		    la.codigo_cta like '1.2.03.04') and la.codigo_cta not like '1.2.02.13' and
                    la.codigo_cta not in ('1.2.02.13','1.2.01.18','1.2.01.22','1.2.01.23','1.2.01.24','1.2.01.25','1.2.01.26','1.2.01.68','1.2.01.72','1.2.01.73','1.2.01.74','1.2.01.75','1.2.01.76','1.2.02.07','1.2.02.08','1.2.02.13','1.2.02.57','1.2.02.58')
                    and a.fecha_asiento between '2003-01-01 00:00:00.000' and '2017-12-31 23:59:59.000'
		    and not exists (select a2.tipo_comprob from SMAndes.dbo.Asientos a2
					    inner join SMAndes.dbo.Lineas_asiento la2 on la2.nro_asiento = a2.nro_asiento
					    where la2.codigo_cta = la.codigo_cta
					    and a2.tipo_comprob in ('NC','FC','CC','ND','OP','OM'))
		    order by codigo_cta, fecha_asiento"""

        query = self.env['ajuste_por_inf.conectar_db'].queryMSSQLPandoraSmAndes(sql_cad)
        return query


    @api.model
    def insert_counts(self):
        rows = self.import_from_sql_server_cuenta_cierre_y_apertura_de_ejercicio()
        for row in rows:
            try:
                count_code, descripcion, debito, credito, year, month, tipo_comprob = row
                # search_row = self.env['ajuste_por_inf.cta_cierre_apertura']

                self.create({
                        'count_code': count_code,
                        'descripcion': descripcion,
                        'debito': debito,
                        'credito': credito,
                        'year': year,
                        'month': month,
                        'tipo_comprob': 'nada' if not tipo_comprob else tipo_comprob})

            except Exception as error:
                # print(error)
                continue

    def lista_agrupada_por_cuentas(self):
        try:
            lista_de_cuentas = [cta.get('count_code').strip() for cta  in \
                    self.env['ajuste_por_inf.cuentacierreapertura'].\
                    read_group(
                        domain=[], fields=['count_code'],
                        groupby=['count_code'], offset=0, lazy=False)]
        except Exception as error:
            lista_de_cuentas = []

        return lista_de_cuentas

    def ajustar_cuentas(self, count_code=0, year=0):
        saldo = 0
        try:
            enero, diciembre = [(f.debito, f.credito) for f in \
                    self.env['ajuste_por_inf.cuentacierreapertura'].search(
                        [('count_code', 'ilike', count_code),
                            ('year', '=', year), '|', ('month', '=', 1),
                            ('month', '=', 12)], limit=2)]
            if sum(enero) == sum(diciembre) and (sum(enero) + sum(diciembre)) >0:
                # print( 'Cuenta:{2} - Año:{3} Debito:{0} Credito {1} '.format(sum(enero), sum(diciembre), count_code, year) )
                saldo = sum(enero)
        except Exception as error:
            # print(error)
            # print( 'Cuenta:{2} - Año:{3} Debito:{0} Credito {1} '.format(sum(enero), sum(diciembre), count_code, year) )
            pass
        return saldo

    def procesar(self, ctas=[], anio_desde=0, anio_hasta=0):
        arreglo = []

        if not ctas:
            # Buscar en "ajuste_por_inf.cuentacierreapertura"
            ctas = self.lista_agrupada_por_cuentas()

        for cta in ctas:
            for year in range(anio_desde, anio_hasta):
                try:
                    saldo = self.ajustar_cuentas(cta, year)

                    # El saldo puede vennir positivo o negativo por eso lo
                    # unico que no  debe pasar son aquellos que sean exatamente
                    # igual a 0
                    # if saldo > 0 or saldo <0:
                    arreglo.append([cta, year,saldo])
                except Exception as error:
                    continue
        # print(arreglo)
        return arreglo

    def  eliminar_saldos_meses_diferente_a_dic(self, cuentas_afectadas):
        # Una condicio diferente al resto, solo apra esa cuenta no deben
        # eliminarse los registros intermedios de enero a noviembre lo ideal
        # seria crer un metodo donde alacene todas las cuentas a las que quiero
        # que no se le aplique la opcion de eliminar meses diferentes a 12


        lista_de_cuentas_a_eliminar = []

        # Si deseo que esta condicion no aplique para algunas cuentas entonces
        # agregar a la lista  ctas_a_remover la cuenta que dseo que no pase
        # nada
        # ctas_a_remover = ['3.2.01.01']
        # [cuentas_afectadas.remove(cta) for cta in ctas_a_remover]

        sql_cad = """
        SELECT la.codigo_cta, la.debito, la.credito, YEAR(a.fecha_asiento) as ANIO,   MONTH(a.fecha_asiento) as MES, a.tipo_comprob
        FROM SMAndes.dbo.Lineas_asiento la
        inner join SMAndes.dbo.Asientos a (nolock) on la.nro_asiento = a.nro_asiento
        where (la.codigo_cta like '1.2.03.04' or
        la.codigo_cta like '1.2.01.%' or la.codigo_cta like '1.2.02.%' or la.codigo_cta like '1.2.03.04' or la.codigo_cta like '3.1.01.%'
        or la.codigo_cta like '3.2.01.%' or la.codigo_cta like '1.2.03.04') and
        la.codigo_cta not in ('1.2.02.13','1.2.01.18','1.2.01.22','1.2.01.23','1.2.01.24','1.2.01.25','1.2.01.26','1.2.01.68','1.2.01.72','1.2.01.73',
        '1.2.01.74','1.2.01.75','1.2.01.76','1.2.02.07','1.2.02.08','1.2.02.13','1.2.02.57','1.2.02.58') and
        a.fecha_asiento between '2003-01-01 00:00:00.000' and '2017-12-31 23:59:59.000'
        and a.tipo_comprob like 'CPEJ' and (YEAR(a.fecha_asiento) = 2003 or YEAR(a.fecha_asiento) = 2004) and MONTH(a.fecha_asiento) = 12
        order by codigo_cta, fecha_asiento
        """

        rows = self.env['ajuste_por_inf.conectar_db'].queryMSSQLPandoraSmAndes(sql_cad)

        # Recorrer las cuentas afectadas y buscar en el arreglo obtenido de la
        # consulta a msSQL, busca los cierres del año 2003 y 2004 para comparar
        # si son iguales: queda  lo que tiene el año 2003 Dic y borro todo de
        # enero a noviemnre, si son difefentes no se hace nadav

        for cta in cuentas_afectadas:
            try:
                a_2003, a_2004 = [f for f in rows if f[0].strip()==cta]
                total_2003  = abs(a_2003[1]) + abs(a_2003[2])
                total_2004  = abs(a_2004[1]) + abs(a_2004[2])

                # import ipdb; ipdb.set_trace() # BREAKPOINT
                if total_2003 == total_2004:
                    lista_de_cuentas_a_eliminar.append(cta)
            except Exception as error:
                continue


        print('Cuentas Eliminadas:', lista_de_cuentas_a_eliminar)
        axi = self.env['account.ajuste_por_inf']
        axi.search([('count_code','in' , lista_de_cuentas_a_eliminar), ('year', '=', 2003), ('month', '<', 12), ('balance', '!=', 0)]).unlink()
        # print('Se elimino la cuenta:', cta)



    def recorrer_cuentcierre_apertura(self):
        cuentas_afectadas = []
        ctas = self.lista_agrupada_por_cuentas()
        for cuenta in ctas:
            lista_de_cuentas = self.insert_saldos([cuenta], 2003, 2018)
            if lista_de_cuentas:
                x = ''.join(list(set(lista_de_cuentas)))
                cuentas_afectadas.append(x)
        print("-------------------------------------------------")
        print('Cuentas Afectadas', cuentas_afectadas)

        # Eliminar saldos meses diferentes a Dic
        self.eliminar_saldos_meses_diferente_a_dic(cuentas_afectadas)

    def  update_saldo_0_anio_2003(self, cta, mes):
        count_code, year, saldo = cta
        # mes = 12
        # anio = 2003

        # Buscar lo que tiene 2004  para obtne saldo y balance en
        # este model

        # De ajuste_por_inf solo obtener el indice del 2003 del mes
        # 12
        axi_2003 = self.env['account.ajuste_por_inf'].search([
            ('count_code', 'ilike', count_code),
            ('year', '=', 2003),
            ('month', '=', 12)], limit=1)

        # De cieere y apertura obtener el debito del mes 1 o
        # credito del mes 12 para calcular el
        # balance del año 2004
        apecierre_2004 = self.search([
            ('count_code', 'ilike', count_code),
            ('year', '=', 2004),
            ('month', '=', mes)], limit=1)


        if axi_2003.exists() and apecierre_2004.exists():
            # si consigue algo entonces en el model ajuste_por Inf
            # reemplazar lo que tenga con los valores del 2004
            ajust_por_inf_2003 = self.env['account.ajuste_por_inf'].search([
                ('count_code', 'ilike', count_code),
                ('year', '=', 2003),
                ('month', '=', 12)], limit=1)

            # print('Ajustar saldo con:', apecierre_2004.debito, apecierre_2004.credito)

            actualizar = ajust_por_inf_2003.write({
                'balance': (apecierre_2004.debito+apecierre_2004.credito),
                'adjustment': (apecierre_2004.debito+apecierre_2004.credito) * axi_2003.index,
                'index': axi_2003.index
                })
            print(actualizar)

        return True

    def  update_saldo_0_otros_anios(self, cta, mes, diferencia):
        count_code, year, saldo = cta
        dominio = [
                ('count_code', 'ilike', count_code),
                ('year', '=', year),
                ('month', '=', mes)]

        cuenta_a_actualizar = self.env['account.ajuste_por_inf'].search(
                dominio, limit=1)
        cuenta_a_actualizar.write({
                'balance': diferencia,
                'adjustment': diferencia * cuenta_a_actualizar.index,
            })
        print('Se actualizo la Cuenta:{0} para el año:{4} mes:{1} - Diferencia:{2} * {3}'.format(count_code, mes, diferencia, cuenta_a_actualizar.index, year))
        return True


    def  update_caso_especial(self, arreglo):
        for cta, year, saldo in arreglo:
            if not saldo:
                domain = [('count_code', 'ilike', cta), ('year', '=', year)]
                try:
                    e = self.env['account.ajuste_por_inf'].search(domain).write(
                            {'balance': 0,
                                'adjustment': 0})
                    print('Borrado:', cta)
                except Exception as error:
                    continue

    def insert_saldos(self, cuenta, desde, hasta ):
        mes = 12
        anio = 2003
        lista = []
        cuentas_afectadas = []
        diferencia = 0

        arreglo = self.procesar(cuenta, desde, hasta)
        # arreglo = self.procesar(['1.2.01.58'], 2003, 2017)

        print(arreglo)
        # Buscar mes que hay que ajustar, es el que esta en 0 y actualizar con year_con_saldo_diferencia
        # Esto se utliza para buscar la diferencia
        # lista = [2659197.24, 4834904.08]
        lista = list(set([f[2] for f in arreglo if f[2]>0]))

        if len(lista) == 1:
            print('caso donde lista == 1', arreglo)

        if len(lista) == 2:
            # Ahora si hay mas de 2 diferencias pero en el año 2003 la suma del
            # debito+credito de Enero es igual a la suma del debito+credito del
            # año 2004 ebtobces hay que aplicarle tambien  el metodo
            # update_saldo_0_anio_2003
            if arreglo[0][2] == arreglo[1][2]:
                _cta = arreglo[0]
                self.update_saldo_0_anio_2003(_cta, mes)

            diferencia = lista[1]-lista[0]

            # Buscar si en account.ajuste_por_inf una cuenta tiene en el año
            # 2003 saldo 0 y colocarle como saldo el mismo que el 2004 en el
            # mes de Dic
            for cta in arreglo:
                count_code, year, saldo = cta

                if saldo == 0 and year == anio:
                    cuentas_afectadas.append(count_code)
                    self.update_saldo_0_anio_2003(cta, mes)

                elif saldo == 0 and year != anio:
                    cuentas_afectadas.append(count_code)
                    year_con_saldo_diferencia = [f[1] for f in arreglo if f[2]==0 and f[1]!=anio]
                    # Aqui hay que buscar en account.ajuste_por_inf y actualizar:
                    # La cuenta, Year, Month=12 e insertar el valor que esta en la
                    # variable diferencia year_con_saldo_diferencia

                    # ajuste =  diferencia * indice
                    if len(year_con_saldo_diferencia) >1:
                        continue

                    print('Diferencia:', diferencia, year_con_saldo_diferencia)
                    # self.update_saldo_0_otros_anios(cta, mes, diferencia)

                    # Cuando hay varios años con diferencia se aplica la misma
                    # regla que cuando la lista>=3
                    # if len(year_con_saldo_diferencia) >1:
                    #     self.update_caso_especial(arreglo)

        if len(lista) >= 3:
            _cta = arreglo[1]
            self.update_saldo_0_anio_2003(_cta, mes)
            cuentas_afectadas.append(_cta[0])
            # self.eliminar_saldos_meses_diferente_a_dic(cuentas_afectadas)
            # self.update_caso_especial(arreglo)

        return cuentas_afectadas

class MovimientoCuentas(models.Model):

    _name = 'ajuste_por_inf.cuentas'
    _order = 'count_code, year, month'


    count_code = fields.Char(string='Codigo Cuenta')
    year = fields.Integer(string='Año')
    month = fields.Integer(string='Mes')
    balance = fields.Float(string='Saldo', default=0.0)

    def borrar_cuentas(self):
        recordset = self.env['ajuste_por_inf.cuentas'].search([])
        recordset.unlink()

    def import_movimientos_por_anio_from_sql_server(self, year=2018):
        """SELECT DE MOVIMIENTOS DE CUENTA solo para año 2018"""

        sql_cad = """
        SELECT la.codigo_cta, c.descrip_cta ,(SUM (la.debito) - SUM (la.credito))  as saldo, YEAR(a.fecha_asiento) as ANIO,   MONTH(a.fecha_asiento) as MES
            FROM SMAndes.dbo.Lineas_asiento la
            inner join SMAndes.dbo.Asientos a (nolock) on la.nro_asiento = a.nro_asiento
            inner join SMAndes.dbo.cuentas c (nolock) on la.codigo_cta = c.codigo_cta
            where
            la.codigo_cta not in ('1.2.02.07','1.2.02.08','1.2.02.13','1.2.02.57','1.2.02.58')
            and a.fecha_asiento between '{0}-01-01 00:00:00.000' and '{0}-12-31 23:59:59.000'
            group by la.codigo_cta,c.descrip_cta, YEAR(a.fecha_asiento), MONTH(a.fecha_asiento)
            order by codigo_cta""".format(year)

        query = self.env['ajuste_por_inf.conectar_db'].queryMSSQLPandoraSmAndes(sql_cad)
        return query

    def import_from_sql_server(self):
        """SELECT DE MOVIMIENTOS DE CUENTA"""

        sql_cad = """SELECT la.codigo_cta as cuenta, YEAR(a.fecha_asiento) as AÑO,
            MONTH(a.fecha_asiento) as MES, (SUM (la.debito) - SUM (la.credito))  as saldo
            FROM dbo.Lineas_asiento la (nolock)
            inner join dbo.Asientos a (nolock) on la.nro_asiento = a.nro_asiento
            where (la.codigo_cta like '1.2.01.%' or la.codigo_cta like '1.2.02.%' or la.codigo_cta like '1.2.03.04' or la.codigo_cta like '3.1.01.%'
                    or la.codigo_cta like '3.2.01.%' or la.codigo_cta like '3.3.01.%' or la.codigo_cta like '1.2.03.04') and la.codigo_cta not like '1.2.02.13'
                and la.codigo_cta not in ('1.2.02.13','1.2.01.18','1.2.01.22','1.2.01.23','1.2.01.24','1.2.01.25','1.2.01.26','1.2.01.68','1.2.01.72','1.2.01.73','1.2.01.74','1.2.01.75','1.2.01.76','1.2.02.07','1.2.02.08','1.2.02.13','1.2.02.57','1.2.02.58')
                and a.fecha_asiento between '2003-03-01 00:00:00.000' and '2017-12-31 23:59:59.000'
                and (tipo_comprob like 'CC' or tipo_comprob like 'FC' or tipo_comprob like 'NC' or tipo_comprob like 'OP' or tipo_comprob like 'OM'
                    or tipo_comprob like 'ND' or tipo_comprob is null)
            group by la.codigo_cta, YEAR(a.fecha_asiento), MONTH(a.fecha_asiento)
            order by la.codigo_cta, YEAR(a.fecha_asiento), MONTH(a.fecha_asiento)
            """

        query = self.env['ajuste_por_inf.conectar_db'].queryMSSQLPandoraSmAndes(sql_cad)
        return query

    @api.model
    def insert_counts(self):

        rows = self.import_from_sql_server()
        for row in rows:
            try:
                count_code, year, month, saldo = row
                search_row = self.env['ajuste_por_inf.cuentas'].search(
                        [], limit=1)

                search_row.create({
                        'count_code': count_code,
                        'year': year,
                        'month': month,
                        'balance': saldo})

            except Exception as error:
                print(error)
                continue

    @api.model
    def group_by_count(self):
        counts = []
        counts_raw = self.env['ajuste_por_inf.cuentas'].read_group(
                domain=[],
                fields=['count_code'],
                groupby=['count_code'],
                offset=0,
                orderby='count_code',
                lazy=False)

        counts = [cuenta.get('count_code').strip() for cuenta in counts_raw]
        return counts

class ajuste_por_inf(models.Model):
    _name = 'account.ajuste_por_inf'

    count_code = fields.Char(string='Codigo Cuenta')
    descripcion = fields.Char(string='Descripcion')
    year = fields.Integer(string='Año')
    month = fields.Integer(string='Mes')
    index = fields.Float(string='Indice', digits=(6, 4), default=0.0)
    balance = fields.Float(string='Saldo', default=0.0)
    adjustment = fields.Float(string='Ajuste', default=0.0)


    def borrar_todo(self):
        recordset = self.env['account.ajuste_por_inf'].search([], limit=500000)
        recordset.unlink()
        return True

    def buscar_indiceo_en_model_indice(self, anio, mes):
        indice = 0
        dominio = [('year', '=', anio),
                ('month', '=', mes)]

        recordset_indices = self.env['ajuste_por_inf.indices'].search(
                dominio, limit=1)

        if recordset_indices:
            indice = recordset_indices.index
        return indice

    def buscar_saldo_en_model_cuenta(self, cuenta, anio, mes):
        saldo = 0
        dominio = [('count_code', 'like', cuenta.strip()),
                ('year', '=', anio),
                ('month', '=', mes)]

        recordset_cuentas = self.env['ajuste_por_inf.cuentas'].search(
                dominio, limit=1)

        if recordset_cuentas:
            saldo = recordset_cuentas.balance
        return saldo

    def buscar_descripcion_en_model_cuentas_reales(self, cuenta):
        descripcion = ''
        dominio = [('count_code', 'like', cuenta.strip())]

        recordset_cuentas = self.env['ajuste_por_inf.cuentas_reales'].search(
                dominio, limit=1)

        if recordset_cuentas:
            descripcion = recordset_cuentas.descripcion
        return descripcion

    def  hacer_ajuste(self, cuenta='', anio=0, mes=0):
        saldo = self.buscar_saldo_en_model_cuenta(cuenta, anio, mes)
        indice = self.buscar_indiceo_en_model_indice(anio, mes)
        try:
            ajuste = saldo * indice
        except Exception as error:
            indice, saldo, ajuste = 0, 0, 0

        return indice, saldo, ajuste

    def insert_rows_empty_por_anio(self, year=2018):
        lista_de_movimientos = self.env['ajuste_por_inf.cuentas'].import_movimientos_por_anio_from_sql_server(year)
        aju_x_inf = self.env['account.ajuste_por_inf']

        for record in lista_de_movimientos:
            cuenta, descripcion, saldo, anio, mes = record
            indice = aju_x_inf.buscar_indiceo_en_model_indice(anio, mes)
            ajuste = float(saldo) * indice

            creado = self.create({
                    'count_code': cuenta,
                    'descripcion': descripcion,
                    'year': anio,
                    'month': mes,
                    'index': indice,
                    'balance': saldo,
                    'adjustment': ajuste})
            print('Creada la cuenta:', cuenta, anio, mes, ajuste)



    def insert_rows_empty(self):
        counts = self.env['ajuste_por_inf.cuentas'].search([],
                limit=1).group_by_count()

        anios = range(2003, 2018)
        meses = range(1,13)

        for cuenta in counts:
            for anio in anios:
                for mes in meses:
                    # print(cuenta, anio, mes)
                    try:

                        descripcion = self.buscar_descripcion_en_model_cuentas_reales(cuenta)

                        saldo = self.buscar_saldo_en_model_cuenta(cuenta,
                                anio, mes)

                        indice = self.buscar_indiceo_en_model_indice(anio, mes)

                        ajuste = saldo * indice

                        search_row = self.env['account.ajuste_por_inf'].search(
                                [], limit=1)
                        search_row.create({
                                'count_code': cuenta,
                                'descripcion': descripcion,
                                'year': anio,
                                'month': mes,
                                'index': indice,
                                'balance': saldo,
                                'adjustment': ajuste})

                    except Exception as error:
                        print(error)
                        continue

    def calcular_ajuste_por_inf(self):
        c = self.env['ajuste_por_inf.cuentacierreapertura']
        c.recorrer_cuentcierre_apertura()
        print('Hola')

class CuentaRelaes(models.Model):
    _name = 'ajuste_por_inf.cuentas_reales'
    _order = 'count_code'

    count_code = fields.Char(string='Codigo Cuenta')
    descripcion = fields.Char(string='Codigo Cuenta')

    def borrar_cuentas(self):
        recordset = self.search([])
        recordset.unlink()

    def import_cuentas_from_sql_server(self):

        sql_cad = """SELECT la.codigo_cta, la.descrip_cta FROM SMAndes.dbo.cuentas la (nolock)
        where (la.codigo_cta like '1.2.03.04' or la.codigo_cta like '1.2.01.%' or la.codigo_cta like '1.2.02.%' or
        la.codigo_cta like '1.2.03.04' or la.codigo_cta like '3.1.01.%' or
        la.codigo_cta like '3.2.01.%' or la.codigo_cta like '1.2.03.04')
        and la.codigo_cta not like '1.2.02.13'
        and la.codigo_cta not in ('1.2.02.13','1.2.01.18','1.2.01.22','1.2.01.23','1.2.01.24','1.2.01.25','1.2.01.26','1.2.01.68','1.2.01.72','1.2.01.73','1.2.01.74','1.2.01.75','1.2.01.76','1.2.02.07','1.2.02.08','1.2.02.13','1.2.02.57','1.2.02.58')
        order by codigo_cta"""

        query = self.env['ajuste_por_inf.conectar_db'].queryMSSQLPandoraSmAndes(sql_cad)
        return query

    @api.multi
    def insert_counts(self):

        rows = self.import_cuentas_from_sql_server()
        for row in rows:
            try:
                count_code, descripcion = row

                self.create({
                        'count_code': count_code,
                        'descripcion': descripcion})

            except Exception as error:
                print(error)
                continue



class SaldoCuenta(models.Model):
    _name = 'ajuste_por_inf.saldo_cuenta'
    _order = 'count_code, year, month'

    count_code = fields.Char(string='Codigo Cuenta')
    year = fields.Integer(string='Año')
    month = fields.Integer(string='Mes')
    balance = fields.Float(string='Saldo', default=0.0)
    index = fields.Float(string='Indice', default=7.5749)
    adjustment = fields.Float(string='Ajuste', default=0.0)

    def borrar_saldo_cuenta(self):
        recordset = self.env['ajuste_por_inf.saldo_cuenta'].search([])
        recordset.unlink()

    def import_from_sql_server(self):

        sql_cad = """SELECT la.codigo_cta, la.descrip_cta FROM SMAndes.dbo.cuentas la (nolock)
        where (la.codigo_cta like '1.2.03.04' or la.codigo_cta like '1.2.01.%' or la.codigo_cta like '1.2.02.%' or
        la.codigo_cta like '1.2.03.04' or la.codigo_cta like '3.1.01.%' or
        la.codigo_cta like '3.2.01.%' or la.codigo_cta like '1.2.03.04')
        and la.codigo_cta not like '1.2.02.13'
        and la.codigo_cta not in ('1.2.02.13','1.2.01.18','1.2.01.22','1.2.01.23','1.2.01.24','1.2.01.25','1.2.01.26','1.2.01.68','1.2.01.72','1.2.01.73','1.2.01.74','1.2.01.75','1.2.01.76','1.2.02.07','1.2.02.08','1.2.02.13','1.2.02.57','1.2.02.58')
        order by codigo_cta"""

        query = self.env['ajuste_por_inf.conectar_db'].queryMSSQLPandoraSmAndes(sql_cad)

        return query

    def import_from_sql_server_enero_y_febrero(self):

        sql_cad = """ SELECT la.codigo_cta,YEAR(a.fecha_asiento) as ANIO,   MONTH(a.fecha_asiento) as MES, (SUM(la.debito) - SUM(la.credito)) as saldo,
	7.5749 as indice, ((SUM(la.debito) - SUM(la.credito))* 7.5749) as ajuste
	FROM SMAndes.dbo.Lineas_asiento la
	inner join SMAndes.dbo.Asientos a (nolock) on la.nro_asiento = a.nro_asiento
	inner join SMAndes.dbo.cuentas c (nolock) on la.codigo_cta = c.codigo_cta
	where (la.codigo_cta like '1.2.03.04' or la.codigo_cta like '1.2.01.%' or la.codigo_cta like '1.2.02.%' or
	la.codigo_cta like '1.2.03.04' or la.codigo_cta like '3.1.01.%' or
	la.codigo_cta like '3.2.01.%' or la.codigo_cta like '1.2.03.04')
        and la.codigo_cta not in ('1.2.02.13','1.2.01.18','1.2.01.22','1.2.01.23','1.2.01.24','1.2.01.25','1.2.01.26','1.2.01.68','1.2.01.72','1.2.01.73','1.2.01.74','1.2.01.75','1.2.01.76','1.2.02.07','1.2.02.08','1.2.02.13','1.2.02.57','1.2.02.58')
	and a.fecha_asiento between '2003-01-01 00:00:00.000' and '2003-02-28 23:59:59.000'
	group by la.codigo_cta, YEAR(a.fecha_asiento), MONTH(a.fecha_asiento)
	order by la.codigo_cta, YEAR(a.fecha_asiento), MONTH(a.fecha_asiento)"""

        query = self.env['ajuste_por_inf.conectar_db'].queryMSSQLPandoraSmAndes(sql_cad)

        return query

    @api.model
    def insert_counts(self):

        rows = self.import_from_sql_server_enero_y_febrero()
        for row in rows:
            try:
                count_code, year, month, saldo, index, ajuste,  = row
                if not saldo:
                    continue
                self.create({
                        'count_code': count_code,
                        'year': year,
                        'month': month,
                        'balance': saldo,
                        'index': index,
                        'adjustment': ajuste})

            except Exception as error:
                print(error)
                continue

    @api.model
    def limpiar(self):
        # Esta vez solo tomamos año 2003 y mes enero y febrero, primero buscar
        # en febrero y si no hay nada entonces tomar el saldo de enero, en caso
        # de conseguir en ambos meses es decir Enero y Ferbrero sumarlos y
        # dejaelo en Feb.

        agrupa = self.read_group(domain=[],
                fields=['count_code'],
                groupby=['count_code'],
                offset=0,
                lazy=False)

        for f in agrupa:
            if f.get('__count') >=2:

                count_code = f.get('count_code')

                # Sumo Enero y  Feb
                ene, feb =  [(f.balance, f.index) for f in self.search(
                    [('count_code', 'ilike', count_code)]
                    )]
                suma_ene_feb = ene[0] + feb[0]
                indice = ene[1] if ene[1] else feb[1]

                # Actualizo Febrero con el nuevo valor
                domain = [
                        ('count_code', 'ilike', count_code),
                        ('month', '=', 2)]

                self.search(domain).write({
                    'balance': suma_ene_feb,
                    'adjustment': (suma_ene_feb * indice)
                    })

                # Elimino Enero
                a_borrar = self.search(
                        [('count_code', '=', count_code),
                            ('month', '!=', 2)])
                a_borrar.unlink()

                print('Cuenta afectada', count_code)

class conectarMSSQL(models.Model):
    _name = "ajuste_por_inf.conectar_db"


    test = fields.Float(digits=(6,2))

    @api.multi
    def queryMSSQLPandoraSmAndes(self, cad_sql):

        def get_db_sql_server_params(host, database, active=True):
            db_parameters = ['', '', '', '']
            db_search = self.env['afip.dbconection'].search([
                ('host', 'ilike', host),
                ('database', 'ilike', database),
                ('active', '=', active)])

            if db_search:
                db_parameters = db_search.host, db_search.user, db_search.password, db_search.database

            return db_parameters

        rows_ = []
        try:
            HOST, USER, PASSWORD, DB = get_db_sql_server_params('pandora', 'smandes')
            conn = pymssql.connect(HOST, USER, PASSWORD, DB)
            cursor = conn.cursor()
            cursor.execute(cad_sql)
            rows_ = cursor.fetchall()
            conn.close()
        except Exception as error:
            pass
        return rows_

