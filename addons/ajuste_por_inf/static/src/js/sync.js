openerp.ajuste_por_inf = function(instance) {
    var ListView = instance.web.ListView;
    ListView.include({
        render_buttons: function() {

        // GET BUTTON REFERENCE
        this._super.apply(this, arguments)
        if (this.$buttons) {
        var btn = this.$buttons.find('.sync_button')
        }

        // PERFORM THE ACTION
        btn.on('click', this.proxy('do_sync'))

        },
        do_sync: function() {
        new instance.web.Model('account.ajuste_por_inf')
            .call('calcular_ajuste_por_inf', [[]])
                .done(function(result) {
                alert('Ajuste realizado con Éxito.')
            })
        }

    });
    ListView.include({
        render_buttons: function() {

        // GET BUTTON REFERENCE
        this._super.apply(this, arguments)
        if (this.$buttons) {
        var btn_2 = this.$buttons.find('.preparar')
        }

        // PERFORM THE ACTION
        btn_2.on('click', this.proxy('do_preparar'))

        },
        do_preparar: function() {
        new instance.web.Model('ajuste_por_inf.indices')
            .call('preparar_ajuste_por_inf', [[]])
                .done(function(result) {
                alert('Listo para proceder a ejecutar ajuste.')
            })
        }

    });

}
