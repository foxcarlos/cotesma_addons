from odoo import models, fields, api

class ParticularReport(models.AbstractModel):
    # Para que este reporte tenga efecto solo hay que colocarle el _name igual
    # al nombre del template del report, por ahora, para que no sea remplazado
    # de le coloca un nombre diferente
    # _name = 'report.ajuste_cote.report_ajuste_template'
    # _name = 'report.ajuste_por_inf.report_ajuste_por_inf'

    @api.model
    def render_html(self, docids, data=None):
        # report_obj = self.env['report']
        # report = report_obj._get_report_from_name('ajuste_por_inf.report_producttemplatelabel')
        docargs = {
            'mi_variable': 'Hola Mundo',
            'docs': [1, 2, 3, 4, 5],
        }
        return self.env['report'].render('ajuste_por_inf.report_ajuste_por_inf', docargs)

        # return report_obj.render('ajuste_por_inf.report_producttemplatelabel', docargs)
