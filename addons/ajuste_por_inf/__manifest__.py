# -*- coding: utf-8 -*-
{
    'name': "ajuste_por_inf",

    'summary': """Modulo que permite hacer el ajuste por inflacion, pasandole
    un archivo excel""",

    'description': """
        Ajuste por inflacion
    """,

    'author': "Carlos Garcia",
    'website': "http://www.cotesna.com.ar",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Accounting',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        "views/web_asset.xml",
        'reports/product_reports.xml',
        'reports/product_pricelist_templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],

    'qweb': ['static/xml/*.xml'],
}
