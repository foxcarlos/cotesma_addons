{
  'name':'Tutorial theme',
  'description': 'Theme de Prueba.',
  'version':'1.0',
  'author':'FoxCarlos',

  'data': ['views/layout.xml',
      'views/pages.xml',
      'views/homepage.xml'
  ],
  'category': 'Theme/Creative',
  'depends': ['website', 'website_blog', 'sale'],
}
