# -*- coding: utf-8 -*-
{
    'name': 'Embed Videos',
    'version': '1.0',
    'website': 'https://www.sorrysemicolon.com',
    'author': 'Alpesh Valaki',
    
    'category': 'Other',
    'summary': 'Embed Videos',
    'description': """this module embed youtube video within odoo form with given video url
""",
    'depends': ['base'],
    'data': [
	    'data/data.xml',
	    'security/ir.model.access.csv',
            'views/video_links.xml',
            'wizard/watch_video_view.xml',
        
    ],
    
    'installable': True,

}
