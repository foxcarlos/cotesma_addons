# Modulos Odoo para Cotesma

Todos los Moculos hechos para Cotesma.

  - Usa Celery y Redis para ejecutar procesos en background para AFIP
  

### Installation

Odoo V10.0 requires python v2 to run.
```sh
$ sudo apt-get install python2.7 python2.7-doc
```

### Installation Redis Server

= Repo =
```sh
$ git@gitlab.com:foxcarlos/cotesma_addons.git
```

= Prepare Virtual Env =
```sh
$ mkdir ~/Envs
$ sudo pip install virtualenvwrapper
$ sudo apt-get install python-virtualenv

Note:Edit File in your Home
$ vim ~.bashrc
export WORKON_HOME=~/Envs
source /usr/local/bin/virtualenvwrapper.sh

Run in terminal
$ mkvirtualenv --python=/usr/bin/python3 para_python3
$ mkvirtualenv para_python2
```

= Install the dependencies =
```sh
$ pip install -r requirements.txt
```

= Para poder ejecutar jobs el archivo de odoo.conf =
```sh
[options]

dbfilter = TuBD
workers = 10
server_wide_modules = web,web_kanban,afip,queue_job
limit_time_real = 9200

[queue_job]
channels = root:50
```

= Install the dependencies =
```sh
El modulo afip requiere instalar el modulo de Odoo "queue_job"
```

./odoo-bin -c odoo2.conf --addons-path=addons,/home/foxcarlos/desarrollo/python/cotesma_addons/addons -d cotesma --log-handler=odoo.addons.queue_job:DEBUG
### Todos

 - Write MORE Tests
 - Add Night Mode

### readme.md Creado con https://dillinger.io/
