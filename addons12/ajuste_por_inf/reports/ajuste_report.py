from odoo import models, fields, api

class ParticularReport(models.AbstractModel):
    _name = 'report.ajuste_por_inf.reportajuste'

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('ajuste_por_inf.reportajuste')
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'mi_variable': 'Hola Mundo',
            'docs': self.env[report.model].browse(docids),
        }
        return report_obj.render('ajuste_por_inf.reportajuste', docargs)
