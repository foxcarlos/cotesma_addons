
from openerp.tests.common import TransactionCase, tagged

from openerp.exceptions import AccessError


@tagged('nice')
class GlobalTestDbconnector(TransactionCase):


    def setUp(self):
        super(GlobalTestDbconnector, self).setUp()
        self.engine = self.env['dbconnnector.engine']

    def create_engine(self, engine_name, description, lib_module, text, active):
        "Create a simple engine."

        engine_id = self.engine.create({
            'name': engine_name,
            'description': description,
            'lib_module': lib_module,
            'text': text,
            'active': active
            })

        return engine_id

    def test_engine_create(self):
        engine_id = self.create_engine(
                'MySQL',
                'MySQL Database',
                'pymysql',
                '',
                True)

        self.assertTrue(engine_id, 'error al crear registro')


