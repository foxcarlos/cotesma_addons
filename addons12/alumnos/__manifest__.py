# -*- coding: utf-8 -*-
{
        "name" : "alumnos",
        "version" : "10.0.0.1",
        "author" : "FoxCarlos",
        "website" : "http://www.cotesma.com.ar",
        "category" : "HR",
        "description": ''' Curso de Odoo ''',
        "depends" : ['base'],
        "demo" : [ ],
        "data" : ['security/ir.model.access.csv', 'views/views.xml', 'views/templates.xml',],
        "installable": True
}



# {
#     'name': "alumnos",
#     'summary': '''
#         Short (1 phrase/line) summary of the module's purpose, used as
#         subtitle on modules listing or apps.openerp.com''',
#     'description': "Registro de Alumnos, Docentes y Talleres",
#     'author': "Francisco Morosini",
#     'website': "http://www.franciscomorosini.com.ar",
#     'category': 'Uncategorized',
#     'version': '1.0',
#     'depends': ['base'],
#     'data': [
#         # 'security/ir.model.access.csv',
#         'views/views.xml',
#         'views/templates.xml',
#     ],
#     'demo': [],
# }
