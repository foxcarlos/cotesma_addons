# -*- coding: utf-8 -*-

from lib.update_guia_telefonica import conectarMSSQL, conectarMySQL


# Borrar todo mySql
def borrar_todo_guiacompleta_mysql():
    mysql = conectarMySQL()
    mysql.query('delete from GuiaCompleta')
    return True

# Buscar registros en MS SQL
def buscar_registros_guiacompleta_sql_server():
    mssql = conectarMSSQL()
    registros = mssql.query('select figuracion, direccion, nro_telefonico from GuiaCompleta')
    return registros

# Actualizar mysql con la inf de ms sql server
def actualizar_registros_guiacompleta_mysql():
    registros = buscar_registros_guiacompleta_sql_server()

    for registro in registros:
        mysql = conectarMySQL()
        conn, cursor, logg, _  = mysql.conectar()
        figuracion, direccion, nro_telefonico = registro
        try:
            cursor.execute("INSERT INTO GuiaCompleta (direccion, figuracion, nro_telefonico) VALUES (%s, %s, %s)", (direccion, figuracion.strip(), nro_telefonico))
            conn.commit()
            conn.close()
            print('Insertando..:', figuracion.strip())
        except Exception as error_query:
            logg.error('{0}'.format(error_query))
            print(error_query)
            conn.close()
            continue

    return True


if __name__ == '__main__':
    print('Main')
    borrar_todo_guiacompleta_mysql()
    actualizar_registros_guiacompleta_mysql()
