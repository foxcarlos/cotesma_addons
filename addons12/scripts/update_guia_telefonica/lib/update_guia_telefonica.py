import pymssql
import mysql.connector as mysql
import logging

class conectarMSSQL(object):

    def __init__(self):
        self.HOST = "fobos"
        self.USER = "webuser"
        self.PASSWD = "HlR4FgFW"
        self.DATABASE = "WEBDB"
        self.log = logging.basicConfig(filename='update_guia_telefonica.log',
                filemode='w',
                format='%(process)d-%(name)s - %(levelname)s - %(asctime)s - %(message)s'
                )

    def conectar(self):
        cursor = ''
        conn = ''
        host, user, passwd, db = self.HOST, self.USER, self.PASSWD, self.DATABASE
        try:
            conn = pymssql.connect(host, user, passwd, db)
            cursor = conn.cursor()
        except Exception as error:
            logging.error('{0}'.format(error))

        return conn, cursor

    def query(self, cad_sql):
        _rows = []
        try:
            conn, cursor = self.conectar()
            cursor.execute(cad_sql)
            _rows = cursor.fetchall()
            conn.close()
        except Exception as error:
            logging.error('{0}'.format(error))

        return _rows


class conectarMySQL(object):

    def __init__(self):
        self.HOST = "jarvis.cotesma.com.ar"
        # self.HOST = "localhost"
        self.USER = "guiatelefonica"
        self.PASSWD = "busqueda47"
        self.DATABASE = "webdb"
        self.log = logging.basicConfig(filename='update_guia_telefonica.log',
                filemode='w',
                format='%(process)d-%(name)s - %(levelname)s - %(asctime)s - %(message)s'
                )

    def conectar(self):
        cursor = ''
        conn = ''
        _error = ''
        host, user, passwd, db = self.HOST, self.USER, self.PASSWD, self.DATABASE
        try:
            db = mysql.connect(host=host,
                    user=user,
                    password=passwd,
                    database=db
                    )
            cursor = db.cursor()
        except Exception as error:
            logging.error('{0}'.format(error))
            _error = error

        return db, cursor, logging, _error

    def query(self, cad_sql):
        _rows = []
        _error = ''
        try:
            conn, cursor, logg, _  = self.conectar()
            cursor.execute(cad_sql)
            if cad_sql.upper().startswith('SELECT'):
                _rows = cursor.fetchall()
            else:
                conn.commit()
            conn.close()
        except Exception as error_query:
            logging.error('{0}'.format(error_query))
            print(error_query)
            print(cad_sql)

        return _rows


