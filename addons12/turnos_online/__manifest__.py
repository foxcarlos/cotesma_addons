

{
    'name': "turnos_online",

    'summary': """
        Sistema de turnos online""",

    'description': """
        Practica curso Carlinos""",

    'author': "RO",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Desconocida',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
         'security/ir.model.access.csv', 'views/modulo_view.xml',
       # 'views/views.xml',
       #'views/templates.xml',
    ],
    # only loaded in demonstration mode

}
