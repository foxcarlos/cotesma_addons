# -*- coding: utf-8 -*-
from odoo import api,fields,models
from odoo.exceptions import ValidationError, UserError
from datetime import datetime

class Medico(models.Model):
	"""Model para el registro de medicos"""
	_description = 'Models de medicos'
	_name = 'turnos_online.medico'
	_rec_name = 'matricula'
	_order = 'apellido desc'

	matricula =fields.Integer(string='Matricula',size = 8, help='Ingrese la matricula provincial (formato 99999999)', required=True)
	nombre =fields.Char(string='Nombre', help='Ingrese el/los nombre/s', required=True)
	apellido =fields.Char(string='Apellido', help='Ingrese el/los apellido/s', required=True)
	telefono =fields.Char(string='Telefono', help="Telefono de contacto", required=True)
	active=fields.Boolean(string='Activo',default=True, help='Activar o desactivar registro')
	foto=fields.Binary(string='Fotografia', help='Seleccione una foto')
	especialidad_ids=fields.Many2many('turnos_online.especialidad',string="Especialidades",required=True)
	prepaga_ids=fields.Many2many('turnos_online.prepaga',string="Prepaga/s" ,required=True)
	agenda_ids = fields.One2many('turnos_online.agenda', 'medico_id', string='Agenda')

	@api.constrains("prepaga_ids")
	def _validate_prepaga_ids(self):
		if not self.prepaga_ids :
			raise ValidationError(
				'Por favor seleccionar al menos una Prepaga'
			)

	@api.constrains("especialidad_ids")
	def _validate_pespecialidad_ids(self):
		if not self.especialidad_ids :
			raise ValidationError(
				'Por favor seleccionar al menos una Especialidad'
			)


class Paciente(models.Model):
	"""Model para el registro de pacientes"""
	_description = 'Models de pacientes'
	_name = 'turnos_online.paciente'
	_rec_name = 'dni'
	_order = 'apellido desc'

	dni =fields.Char(string='DNI',size = 8, help='Ingrese el dni con el formato 99999999')
	nombre =fields.Char(string='Nombres', help='Ingrese primer y segundo nombre')
	apellido =fields.Char(string='Apellidos', help='Ingrese el apellido')
	direccion =fields.Text(string='Direccion', help='Ingrese la direccion')
	genero =fields.Selection([('F','Femenino'),('M','Masculino')],string='Genero', help='Ingrese su sexo')
	email = fields.Char(string='Mail', help="E-mail para notificaciones")
	fecha_nacimiento = fields.Date(string='Fecha de nacimiento',help='Ingrese su fecha de nacimiento con el siguiente formato DD/MM/AAAA')
	telefono_ids=fields.One2many('turnos_online.telefono', 'paciente_id', string="Telefonos")
	_sql_constraints = [('dni_unico','UNIQUE(dni)','El numero de documento debe ser unico')]
	prepaga_ids=fields.Many2many('turnos_online.prepaga',string="Prepaga/s", required=True)

#@api.one es para un metodo que se crea para un campo en el que estoy posicionado
	@api.one
	def mimetodo(self):
		pass

	@api.constrains("fecha_nacimiento")
	def _validate_fecha_nacimiento(self):
		if self.fecha_nacimiento > datetime.now().date():
			raise ValidationError(
				'La fecha de nacimiento "{0}" no puede ser mayor a la fecha de hoy {1}'.format(
				self.fecha_nacimiento,datetime.now().date())
			)

class Especialidad(models.Model):
	"""Especialidades Medicas"""
	_description = 'Models de especialidades'
	_name = 'turnos_online.especialidad'
	_rec_name = 'especialidad'
	_order = 'codigo_esp'

	especialidad=fields.Char(string='Especialidad', help='Especialidad', required=True)
	codigo_esp=fields.Integer(string='Codigo', size=4, help='Codigo internacional de la especialidad', required=True)
	_sql_constraints=[('codigo_esp_unico','UNIQUE(codigo_esp)','El codigo de la especialidad debe ser unico')]

class Prepaga(models.Model):
	"""Prepagas"""
	_description = 'Models de Prepagas'
	_name = 'turnos_online.prepaga'
	_rec_name = 'prepaga'
	_order = 'codigo_prep'

	prepaga = fields.Char(string='Prepaga', help='Prepaga', required=True)
	codigo_prep = fields.Integer(string='Codigo', size=4, help='Codigo de la prepaga', required=True)
	_sql_constraints = [('codigo_prep_unico','UNIQUE(codigo_prep)','El codigo de la prepaga debe ser unico')]

class Telefono(models.Model):
	"""Telefonos"""

	_description = 'Models de Telefono'
	_name = 'turnos_online.telefono'
	_rec_name = 'telefono'
	_order = 'telefono'

	telefono=fields.Char(string='Telefono', help="Telefono")
	paciente_id=fields.Many2one('turnos_online.paciente')

#@api.multi es para aplicar un metodo a varios registros a la vez en lugar de uno

#@api.constrains es para hacer validaciones antes de que odoo haga el save. Estos metodos deben ser privados y eso se determina con un _antes del nombre

class Consultorio(models.Model):
	"""Consultorio"""

	_description = 'Models de Consultorio'
	_name = 'turnos_online.consultorio'
	_rec_name = 'nro'
	_order = 'nro'

	nro=fields.Integer(string='Numero', size=3, help='Numero del consultorio', required=True)
	piso=fields.Char(string='Piso', size=1, help="Piso")
	direccion =fields.Text(string='Direccion', help='Ingrese la direccion')
	_sql_constraints=[('nro_unico','UNIQUE(nro)','El nro del consultorio debe ser unico')]

class Agenda(models.Model):
	"""Agenda de turnos"""

	_description = 'Models de Agenda de turnos'
	_name = 'turnos_online.agenda'
	_rec_name = 'duracion_turno'
	_order = 'fecha_desde, fecha_hasta desc'

	cantidad_max=fields.Integer(string='Cantidad Maxima de Turnos', default=0, size=3, help='Cantidad maxima de turnos por franja horaria')
	medico_id= fields.Many2one('turnos_online.medico',string="Medico", required=True)
	medico_nombre = fields.Char(string='Medico', related='medico_id.nombre', readonly=True)
	consultorio_ids=fields.Many2many('turnos_online.consultorio',string="Consultorio", required=True)
	fecha_desde=fields.Datetime(string='Fecha desde', help='Fecha/Hora inicio')
	fecha_hasta=fields.Datetime(string='Fecha hasta', help='Fecha/Hora fin')
	duracion_turno=fields.Integer(string='Duracion (minutos)', help='Duracion del turno')
	admite_entreturno=fields.Boolean(string='Admite entreturno', default=False, help='Determina si la agenda admite entreturno')
	disponibilidad_ids=fields.One2many('turnos_online.disponibilidad', 'agenda_id', string="Turnos disponibles")
	turno_ids=fields.One2many('turnos_online.turno','agenda_id',string="Turnos ocupados")

	@api.constrains("duracion_turno")
	def _validate_duracion_turno(self):
		if self.duracion_turno > 60:
			raise ValidationError(
				'La duracion "{0}" es invalida'
			)

	def cantidad_max3(self):
		hh = datetime.strptime (self.fecha_hasta, '%d/%m/%Y %H:%M:%S')
		hd = datetime.strptime (self.fecha_desde, '%d/%m/%Y %H:%M:%S')
		dur = self.duracion_turno * 60
		self.cantidad_max = (hh-hd)/dur

	# @api.onchange(duracion_turno)
	# def crear(self):
	# 	pass

	"""@api.one
	def crear_turnos_disp(self):
		for fecha in range (fecha_hasta-fecha_desde)
			for fila in range (1,cantidad_max):
				td = Disponibilidad()"""



class Disponibilidad(models.Model):
	"""Turnos disponibles"""

	_description = 'Models de Turnos disponibles'
	_name = 'turnos_online.disponibilidad'
	_rec_name = 'fecha_turno'
	_order = 'fecha_desde, hora_desde desc'

	agenda_id = fields.Many2one('turnos_online.agenda', string='Agenda')
	fecha_turno = fields.Date(string='Fecha Turno', help='Fecha del turno')
	#hora_turno = fields.time(string='Hora Turno', help='Hora del turno')

class Turno(models.Model):
	"""Solicitud de Turno"""

	_description = 'Models de Solicitudes de turnos'
	_name = 'turnos_online.turno'
	_rec_name = 'fecha_turno'
	_order = 'fecha_turno desc'

	fecha_creacion=fields.Datetime(string='Fecha Creacion', help='Fecha en la que se solicito el turno')
	fecha_turno=fields.Datetime(string='Fecha Turno', help='Fecha en la que se quiere asistir al consultorio')
	agenda_id = fields.Many2one('turnos_online.agenda', string='Agenda')
