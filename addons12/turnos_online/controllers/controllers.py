"""from odoo import http

#es para utilizarlo para cualquier consulta web

class Intranet(http.Controller):
    @http.route('/intranet/', auth='public', website=True)
    def index(self, **kw):
        #Buscar en el Modelos
        personas = http.request.env['mimodulo.persona']
        params = {"personas":personas.search([])}
        return http.request.render('mimodulo.index',params)

    @http.route('/intranet/<name>/', auth='public', website=True)
    def nombre(self,name):
        return '<h1>El nombre de la url es: {0}</h1>'.format(name)

    @http.route('/intranet/<int:id>/', auth='public', website=True)
    def id(self,id):
        return '<h1>El DNI de la url es: {0}</h1>'.format(id)

    @http.route('/intranet/biografia/<model("mimodulo.persona"):nombres>/', auth='public', website=True)
    def biografia(self,nombres):
        params = {'persona':nombres}
        return http.request.render('mimodulo.biografia',params)
"""
