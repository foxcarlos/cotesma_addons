#-*- coding:utf-8 -*-

from odoo.tests.common import TransactionCase

class TestPaciente(TransactionCase):
    def test_create(self):
        "Create a simple test"
        Persona = self.env['turnos_online.paciente']
        insertar = Paciente.create({'dni': '32582611'})

        # self.assertEqual(insertar.active, True)
        self.assertEqual(insertar.active, 'a')
